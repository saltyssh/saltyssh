# SaltySSH

## About SaltySSH

SaltySSH is a tarpit intended to annoy wannabe SSH hackers/scanners. Heavily inspired by Chris _skeeto_ Wellons' [endlessh](https://github.com/skeeto/endlessh).

## Config example

```
### saltyssh.ini

## network settings
[server]
listen_ip=127.0.0.1
listen_port=20202
static_ident=True

## control tx speeds
[speed]
speed_do_random=False
speed_min=5000
speed_max=8000

## geoiplookup stuff
[geoip]
enabled=True
use_local=False
geoip_url='https://ip2c.org/?ip='
```

## License

Distributed under the LGPL 3.0 License. See `LICENSE.txt` for more information.