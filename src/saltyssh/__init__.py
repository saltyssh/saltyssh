##
# SALTYSSH
#
# Copyright (c) 2022 by Duck Hunt-Pr0
# Free software under the terms of the LGPL v3.0 <https://www.gnu.org/licenses/lgpl-3.0.html>
#
##

__version__ = '0.5b'

SSHD_DEFAULT_IF   = '127.0.0.1'
SSHD_DEFAULT_PORT = 20202

SSHD_RANDOMIZED_SPEED = False   # If false, use only SSHD_DEFAULT_MIN_SPEED
SSHD_DEFAULT_MIN_SPEED = 5000   # max ms between sending shit
SSHD_DEFAULT_MAX_SPEED = 5000   # max ms between sending shit

SSHD_IDENT_STRING = 'SSH-2.0-OpenSSH_8.8'
SSHD_IDENT_STATIC = True
# note: https://github.com/0x4D31/hassh-utils/blob/master/hasshdb
SSHD_IDENT_STRINGS = ['SSH-2.0-OpenSSH_5.9p1 Debian-5ubuntu1.1',
                      'SSH-2.0-ssh2js0.0.18',
                      'SSH-2.0-OpenSSH_8.8',
                      'SSH-2.0-',
                      'SSH-1.99-OpenSSH_3.9p1',
                      'SSH-2.0-OpenSSH_7.4p1 Raspbian-10+deb9u2',
                      'SSH-1.5-',
                      'SSH-1.0-lshd-0.5b a GNU ssh',
                      'SSH-2.0-dropbear_0.51',
                      'SSH-2.0-wolfSSHv 1.4.8',
                      'SSH-1.99-OpenSSH_3.9p1',
                      'SSH-2.0-Cisco-1.25',
                      'SSH-2.0-RebexSSH_1.0.2.27069',
                      'SSH-2.0-Trendchip_0.1',
                      'SSH-1.99-BDCOMSSH_2.0.0',
                      'SSH-2.0-OpenSSH_5.9 NetBSD_Secure_Shell-20110907-hpn13v11-lpk',
                      'SSH-2.0-HUAWEI-1.5']

ASCII_FUN_FOLDER = ''   # a folder with some fun ascii textfiles to sometimes use

GEOIP_LOOKUP_USE = True
GEOIP_LOOKUP_USE_LOCAL = False
GEOIP_LOOKUP_API_URL = 'https://ip2c.org/?ip='