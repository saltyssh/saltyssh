##
# SALTYSSH
#
# Copyright (c) 2022 by Duck Hunt-Pr0
# Free software under the terms of the LGPL v3.0 <https://www.gnu.org/licenses/lgpl-3.0.html>
#
##
import asyncio
import logging
from random import uniform
from time import time as timestamp

import saltyssh.garblegen
from . import SSHD_IDENT_STRING
from .config import Config
from .iplookup import geoIPlookup_IP2C as geoIPlookup

## writebuffer limits
WBUFFR_LIM_L = 1024 * 1  # def. 16384
WBUFFR_LIM_H = 1024 * 4  # def. 65536


def SaltySSHDconnection(config: Config, log: logging.Logger):
    log.info("Ready for connections!")

    @asyncio.coroutine
    async def handle_connection(reader, writer):
        writer.transport.set_write_buffer_limits(high=WBUFFR_LIM_H, low=WBUFFR_LIM_L)

        num_bytes_total: int = 0

        addr = writer.get_extra_info('peername')
        country = geoIPlookup(addr[0])
        start_timestamp = timestamp()
        log.info("CONNECT| {ip}:{port} cntry='{cntry}'".format(ip=addr[0],
                                                            port=addr[1],
                                                            cntry=country))
        if config._static_ident is True:
            ident_string = SSHD_IDENT_STRING
        else:
            ident_string = saltyssh.garblegen.getRandIdentStr()

        #writer.write(bytes(ident_string + ' ', 'UTF-8'))

        end_timestamp = timestamp()
        #log.info("SEND| Ident string '{identstr}' to ip={ip}:{port} cntry='{cntry}'".format(identstr=ident_string, ip=addr[0], port=addr[1],cntry=country))

        buffr_drain_limit: int = 32  # WBUFFR_LIM_L  # run writer.drain() when passing this(?)
        buffr_written: int = 0
        while True:
            log.debug("Transport buffer {s} / L{0}l H{1}".format(*writer.transport.get_write_buffer_limits(),
                                                                 s=writer.transport.get_write_buffer_size()))
            # to_send = bytes('chingchong ', 'UTF-8')  # '\033[s' + '\033[u'
            to_send = bytes(saltyssh.garblegen.getRandStr(8, 12), 'UTF-8')  # '\033[s' + '\033[u'
            num_bytes = len(to_send)

            log.debug("PAYLOAD| ip={ip}:{port} payload=\n\n{plod}\n".format(plod=to_send,
                                                                 ip=addr[0],
                                                                 port=addr[1],
                                                                 num_bytes=len(to_send)))

            try:
                writer.write(to_send)
                log.info("SEND| ip={ip}:{port} byt={sendtbytes:d} totbyt={totalbytes:d} twasted={waste:.3f} cntry='{cntry}'".format(
                    ip=addr[0],
                    port=addr[1],
                    sendtbytes=num_bytes,
                    totalbytes=num_bytes_total,
                    waste=(end_timestamp - start_timestamp),
                    cntry=country
                ))
                buffr_written += num_bytes

                # if buffr_written >= buffr_drain_limit:
                if True:  # todo: not using drain() each time cause of socket.send() exceptions? enable for now
                    log.debug("Doing writer.drain()")
                    await writer.drain()
                    buffr_written = 0

            except BrokenPipeError:
                log.debug("Broken pipe to {ip} {port}".format(ip=addr[0], port=addr[1]))
                break
            except ConnectionResetError:
                log.debug("Connection closed by {ip}:{port}".format(ip=addr[0], port=addr[1]))
                break
            except InterruptedError:
                log.debug("Socket.send() exception?")

            end_timestamp = timestamp()
            num_bytes_total += num_bytes

            if config._speed_do_random == True:
                sleep_time = uniform(config._speed_min, config._speed_max)  # todo: use config.get*() for config values
            else:
                sleep_time = config._speed_min

            log.debug("Sleeping {s:.3f}s for ip={ip}:{port} cntry='{cntry}'".format(s=sleep_time,
                                                                    ip=addr[0],
                                                                    port=addr[1],
                                                                    cntry=country)
                      )
            await asyncio.sleep(round(float(sleep_time)))  # todo: use config.get*() for config values

        end_timestamp = timestamp()
        wasted_time = end_timestamp - start_timestamp
        log.info("CLOSE| ip={ip}:{port} totbyt={totalbytes:d} totwasted={waste:.3f} cntry='{cntry}'".format(ip=addr[0],
                                                                                             port=addr[1],
                                                                                             waste=wasted_time,
                                                                                             totalbytes=num_bytes_total,
                                                                                            cntry=country

                                                                                             ))
        # await writer.drain()
        writer.close()

        # except ConnectionResetError:
        #     print("Connection lost!")
        #
        # finally:
        #     try:

        # print('Server done')
        # except RuntimeError:
        #     print("jalla!")

    loop = asyncio.get_event_loop()
    log.debug("AsyncIO event loop set..")
    coro = asyncio.start_server(handle_connection, config._listen_ip, config._listen_port, loop=loop)
    log.debug("AsyncIO coroutine set..")

    log.debug("AsyncIO event loop server set..")
    server = loop.run_until_complete(coro)
    log.debug("AsyncIO event loop server complete..")

    # print('Serving on {}'.format(server.sockets[0].getsockname()))

    try:
        log.debug("AsyncIO event loop run forever..")
        loop.run_forever()
    except KeyboardInterrupt:
        log.info("QUIT| CTRL-C Detected! Quitting...")
        log.debug("AsyncIO event loop stopping..")
        loop.stop()
        log.debug("AsyncIO event loop stopped..")
    # while loop.is_running():
    #    pass
    # loop.close()

# SaltySSHDserver = multiprocessing.Process(target=SaltySSHDconnection, name='SaltySSHD')
