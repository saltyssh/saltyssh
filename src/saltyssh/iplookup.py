from urllib import request
from urllib.parse import urlencode
## use '{ip}' as template where
# the ip to lookup will be inserted
#;geoip_url='freegeoip.net/xml/{ip}'
#;geoip_url='https://ip2c.org/?ip={ip}'
#;geoip_url='https://api.hackertarget.com/geoip/?q={ip}'


def geoIPlookup_IP2C(ip,url_template:str=''):
    # todo: implement being able to use alternative lookup sites from .ini config
    urlstring='https://ip2c.org/?ip={ip}'.format(ip=ip)
    #print(urlstring)
    with request.urlopen(urlstring) as response:
        data = response.read()
        response.close()
    try: country = data.decode().split(';')[-1]
    except IndexError: country = "UNKNOWN"
    #print(data)
    return str(country)


def geoIPlookup_HackerTarget(ip,url_template:str=''):
    # todo: implement being able to use alternative lookup sites from .ini config
    urlstring='https://api.hackertarget.com/geoip/?q={ip}'.format(ip=ip)
    #print(urlstring)
    with request.urlopen(urlstring) as response:
        data = response.read()
        response.close()
    data = data.decode("utf-8").split('\n')
    try: country = data[1].split(':')[1].lstrip(' ')
    except IndexError: country = "UNKNOWN"
    return str(country)

#geoIPlookup=geoIPlookup_IP2C
#geoIPlookup=geoIPlookup_HackerTarget
#print(geoIPlookup('112.85.42.15'))